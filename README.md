# COFFEE BILLING

This is used to display  the billing process.

## Used technologies
---
<ul>
    <li>Html is used to made the structure how to appear.</li>
    <li>CSS is used to add styles.</li>
</ul>

## CHALLENGES
---
<ul>
    <li>To create boxes and align them together.</li>
    <li>Making different and align them in a proper manner.</li>
    <li>To display them in a proper manner.</li>
</ul>

## Changes which I want to do in future
---
<ul>
    <li>To made it more efficient.</li>
</ul>

## Visit this site
---

URL : https://silver-quokka-c358ef.netlify.app
